**introduction**
**AIM**

The Learner will be able to<br><br>
<u> **<span style="color:blue"> 1. demonstrate working of PHP code</span>** </u><br>
<u> **<span style="color:blue"> 2. create basic PHP scripts using proper syntax</span>** </u>

**theory**<br>
PHP is widely used as an open source scripting language. PHP is an acronym for Hypertext Preprocessor. PHP scripts are executed on server and the HTML result is sent back to the client (Browser).PHP can generate dynamic page content. It can work with files on the server and can be used to collect form data. PHP can send and receive cookies. It can also work with database and can be easily embedded in HTML.PHP is widely used for Web Development.
PHP script generally contains HTML tags and PHP script.
 Every PHP script is stored with .php extension. PHP script can run on any platform (Windows, Linux,etc) and is compatible with most of the web servers (Apache,IIS,etc.). PHP has a support for wide range of databases with which one can work.
To start using PHP, we need a web server (host) with PHP and MySQL support along with PHP and MySQL installed on your PC or Laptop.
PHP script can be placed anywhere in the document. PHP script starts with <?php and ends with?>.The script written between these two tags is parsed on the server and the plain HTML result is returned back. All php statements must end with **semicolon ( ; )**<br><br>

**procedure**

To run the script one needs a server enabled with PHP and a browser. PHP is loosely typed language.
echo or print is used to print the output.
E.g.following script will print "Welcome to PHP"
< ?php<br>
$number_1=10;<br>
$number_2=35.5;<br>
$str ="HELLO";<br>
echo $number_1;<br>

echo $number_2;<br>
echo $str;<br>
?><br>
**Click the "Execute PHP" button and observe:**


[Link Text](link "Title")<br>
2.Observe the location of the yellow band. It highlights the line being executed.<br>
3.Observe the output shown on the right hand side.<br>
4.Observe the html code generated during the execution at the server side, displayed below the PHP code

